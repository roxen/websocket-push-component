package com.example;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.wicket.Application;
import org.apache.wicket.ajax.WebSocketRequestHandler;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;
import org.apache.wicket.protocol.ws.api.IWebSocketConnectionRegistry;
import org.apache.wicket.protocol.ws.api.SimpleWebSocketConnectionRegistry;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.TextMessage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class HomePage extends WebPage {
    private static final long serialVersionUID = 1L;
    private Label label;

    private String applicationName;
    private String sessionId;
    private Integer pageId;

    public HomePage(final PageParameters parameters) {
        super(parameters);

        add(new Label("version", getApplication().getFrameworkSettings().getVersion()));

        label = new Label("label", "Original text");
        label.setOutputMarkupId(true);
        add(label);

        add(new WebSocketBehavior() {

            private static final long serialVersionUID = 1L;

            @Override
            protected void onConnect(ConnectedMessage message) {
                applicationName = message.getApplication().getName();
                sessionId = message.getSessionId();
                pageId = message.getPageId();
            }

            @Override
            protected void onMessage(WebSocketRequestHandler handler, TextMessage message) {
                label.setDefaultModelObject(message.getText());
                handler.add(label);
            }
        });

        UpdateTask updateTask = new UpdateTask();
        Executors.newScheduledThreadPool(1).schedule(updateTask, 1, TimeUnit.SECONDS);
    }

    public class UpdateTask implements Runnable {
        int count = 0;

        @Override
        public void run() {
            while (true) {
                try {

                    if (applicationName == null) {
                        Thread.sleep(200);
                        continue;
                    }

                    Application application = Application.get(applicationName);

                    IWebSocketConnectionRegistry webSocketConnectionRegistry = new SimpleWebSocketConnectionRegistry();
                    IWebSocketConnection connection = webSocketConnectionRegistry.getConnection(application, sessionId,
                            pageId);
                    if (connection == null || !connection.isOpen()) {
                        System.out.println("no connection in loop...");
                        return;
                    }

                    try {
                        connection.sendMessage("hi from server " + count++);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
